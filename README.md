# 1K2S's Projects

[![Netlify Status](https://api.netlify.com/api/v1/badges/a7cccd56-43ac-49f4-abd7-38c56ba83f18/deploy-status)](https://1k2s.netlify.app)

This group was made to showcase some of my more important projects. You can additionally
check out the showcase on the group website:
[1k2s.netlify.app](https://1k2s.netlify.app).

## List Of Projects

This is the comprehensive list of the current projects:

### [Yew Site](https://gitlab.com/1k2s/yew-site)

<!-- Using <p> instead of <div> for the line break at the end. -->
<p>
  <img height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Yew-Light.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Sass.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Python-Dark.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/HTML.svg"
  />
</p>

This is the actual visitable site which contains the project's showcases as
well as an image gallery and a blogging page. You can think of it as the
homepage of this project. It showcases most of the projects listed here but
not all of them are in the site's showcase list.

### [Discord Interactions Bot](https://gitlab.com/1k2s/discord-interactions-bot)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/TypeScript.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/NodeJS-Light.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Npm-Light.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/MongoDB.svg"
  />
</p>

This was my first Discord bot which was made via `Discord.JS` using 
`TypeScript` with `NodeJS` and `npm`. The database is `MongoDB` which works
very well with `NodeJS` projects.

### [Serenity Discord Bot](https://gitlab.com/1k2s/serenity-discord-bot)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/SQLite.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Docker.svg"
  />
</p>

This is a revised version of the original
[discord interactions bot](https://gitlab.com/1k2s/discord-interactions-bot)
which was my first poor attempt at making a bot that allows for user
interactions. This version uses `SQLite` (instead of `MongoDB`) as well as 
`Rust` via the `Poise` and Serenity frameworks (instead of `TypeScript` via 
`Discord.JS`) as the primary language and frameworks. The current one also has
a `Docker` and `Docker Compose` configuration.

### [Hunger Games Website](https://gitlab.com/1k2s/hunger-games-website)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/TypeScript.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/CSS.svg"
  />
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/HTML.svg"
  />
</p>

This website was inspired from
[this site](https://simublast.com/hunger-games-simulator/) and is hosted on
[GitLab pages](https://1k2s.gitlab.io/hunger-games-website/). It's built with
`Typescript`, `HTML` and `CSS`.

### [LeetCode Trees](https://gitlab.com/1k2s/leetcode-trees-rs)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
</p>

A `Rust` library which uses Cargo make for the solutions directory and the
library itself contains required LeetCode structs signatures such as:
`TreeNode` and the `ListNode` signatures. Additionally, the library provides
the users with powerful macros which excel the ease of code generation.

### [Counting blinks](https://gitlab.com/1k2s/counting-blinks)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
</p>

A `Rust` app (via the `arduino_hal` crate) that works with 3 blinking leds
which implement the basic binary counting (implemented via bitshifting).

### [Rust RPS](https://gitlab.com/1k2s/rust-rps)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
</p>

A simple `CLI` `Rust` Rock Paper Scissors game implemented using Enums,
HashMaps and parsing from the user input.

### [Matrix Math](https://gitlab.com/1k2s/matrix-math)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
</p>

A `Rust` attempt of a library that does Matrix operations. It's incredibly
sub-optimal as it uses double heap indirection in the form of: `Vec<Vec<T>>`.

### [Rust OS](https://gitlab.com/1k2s/rust-os)

<p>
  <img
    height="50px"
    src="https://codeberg.org/1Kill2Steal/skill-icons/raw/branch/main/icons/Rust.svg"
  />
</p>

A basic VGA output displaying operating system built in `Rust` which can't even
compile on my machine.